<?

namespace Projetc\Sync\Event;

use Projetc\Sync\Platform\Update;

class Platform {

    public static function OnAfterIBlockPropertyUpdate($arFiles) {
        Update::hl($arFiles);
//        preExit(func_get_args());
    }

    public static function OnAfterAdd($event) {
        $arFiles = $event->getParameters()['fields'];
        Update::props($arFiles['UF_NAME'], $arFiles['UF_XML_ID']);
//        preExit();
    }

    public static function OnAfterUpdate($event) {
        $arFiles = $event->getParameters()['fields'];
        Update::props($arFiles['UF_NAME'], $arFiles['UF_XML_ID']);
//        preExit();
    }

    public static function OnBeforeDelete($event) {
        Update::deleteProps($event->getParameters()['id']['ID']);
//        preExit();
    }

}
