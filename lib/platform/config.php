<?php

namespace Projetc\Sync\Platform;

class Config {

    const HL = 2;
    const HL_CODE = 'Platform';
    const IBLOCK_ID = 10;
    const PROPERTY_ID = 32;

}
